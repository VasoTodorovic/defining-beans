package com.example.definig.beans;

public class ParrotProgrammatically {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
