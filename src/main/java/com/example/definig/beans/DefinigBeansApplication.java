package com.example.definig.beans;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.function.Supplier;

@SpringBootApplication
public class DefinigBeansApplication {

    public static void main(String[] args) {
        var context = new AnnotationConfigApplicationContext(ProjectConfig.class);

        ParrotBean p1 = context.getBean("parrot1", ParrotBean.class);

        System.out.println(p1.getName());

        String s = context.getBean(String.class);

        System.out.println(s);

        Integer n = context.getBean(Integer.class);

        System.out.println(n);
        ParrotStereotype parot4 = context.getBean(ParrotStereotype.class);
        parot4.setName("I am Ziki,parrotStereotype");
        System.out.println(p1);
        System.out.println(p1.getName());


       //1
		String stringProgrammatically = "string Programmatically";
		//2
		Supplier<String> stringSupplier = () -> stringProgrammatically;


		//3
        context.registerBean("string",
                String.class,
                stringSupplier,
                bc -> bc.setPrimary(true));

		String s1=context.getBean(String.class);
		System.out.println(s1);

       //1
        ParrotProgrammatically x = new ParrotProgrammatically();
        x.setName("Biki");
       //2
        Supplier<ParrotProgrammatically> parrotSupplier = () -> x;
		//3
		context.registerBean("parrot5",
				ParrotProgrammatically.class,
				parrotSupplier,
                bc -> bc.setFactoryBeanName("parrot5")
				);


		ParrotProgrammatically p = context.getBean("parrot5",ParrotProgrammatically.class);

        System.out.println(p.getName());

    }
}
