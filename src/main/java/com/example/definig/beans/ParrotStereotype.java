package com.example.definig.beans;

import org.springframework.stereotype.Component;

@Component
public class ParrotStereotype {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}