package com.example.definig.beans;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
@ComponentScan(basePackages = "com/example/definig/beans")
@Configuration
public class ProjectConfig {

    @Bean
    ParrotBean parrot1() {
        var p = new ParrotBean();
        p.setName("Koko");
        return p;
    }

    @Bean
    String hello() {
        return "Hello";
    }

    @Bean
    Integer ten() {
        return 10;
    }

    @Bean
    ParrotBean parrot2() {
        var p = new ParrotBean();
        p.setName("Miki");
        return p;
    }

    @Bean
    ParrotBean parrot3() {
        var p = new ParrotBean();
        p.setName("Riki");
        return p;
    }
}
