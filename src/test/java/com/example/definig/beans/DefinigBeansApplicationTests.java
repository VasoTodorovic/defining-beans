package com.example.definig.beans;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DefinigBeansApplicationTests {

	@Autowired
	private ApplicationContext context;
	@Test
	@DisplayName("Test that Parrot instance named parrot1 has the name Koko.")
	public void testParrot1HasTheNameKoko() {
		ParrotBean p = context.getBean("parrot1", ParrotBean.class);

		assertEquals("Koko", p.getName());
	}

	@Test
	@DisplayName("Test that Parrot instance named parrot2 has the name Miki.")
	public void testParrot2HasTheNameMiki() {
		ParrotBean p = context.getBean("parrot2", ParrotBean.class);

		assertEquals("Miki", p.getName());
	}

	@Test
	@DisplayName("Test that Parrot instance named parrot3 has the name Riki.")
	public void testParrot3HasTheNameRiki() {
		ParrotBean p = context.getBean("parrot3", ParrotBean.class);

		assertEquals("Riki", p.getName());
	}
	@Test
	public void testHelloIsInTheSpringContext() {
		String s = context.getBean(String.class);

		assertEquals("Hello", s);
	}
	@Test
	public void test10IsInTheSpringContext() {
		Integer i = context.getBean(Integer.class);

		assertEquals(10, i);
	}

}
