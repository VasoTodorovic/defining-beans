# Defining beans
## About project 

The project contains three ways of adding beans to the Spring context.
The readme explains the project and it's represent **my** notes from book  [Spring start here](https://www.manning.com/books/spring-start-here)

## Spring framework
An application framework is a set of functionalities on top of which we build applications. The application framework provides us a broad set of tools and functionalities that you can use to build apps. You don’t need to use all the features the framework offers. Depending on the requirements of the app you make, you’ll choose the right parts of the framework to use.

Usually, when developers refer to the Spring framework, they refer to a part of the software capabilities that include the following:
* Spring Core
Spring context enables Spring to manage instances of your app
help Spring intercept and manipulate methods you define in your app

* Spring model-view-controller (MVC)—enables you to develop web applications that serve HTTP requests
* Spring Data Access—Also one of the fundamental parts of Spring. It provides basic tools you can use to connect to SQL databases
* Spring testing—The part holding the tools you need to write tests


Spring works based on the principle inversion of control (IoC)
nstead of allowing the app to control the execution, we give control to some other piece of software—in our case, the Spring framework
you don’t let the app control the execution by its own code and use dependencies. Instead, we allow the framework (the dependency)


![a]( images/1.png "a")
Spring framework controls an app during its execution. Therefore, it implements an IoC scenario of execution.

Based on the configurations that you write, Spring intercepts the method to augment it with various features

IoC container glues Spring components and components of your application to the framework together. Using the IoC container, to which you often refer as the Spring context, you make certain objects known to Spring, which enables the framework to use them in the way you configured.

## The Spring context
To enable Spring to see your objects, you need to add them to the context.

* how to add object instances to the Spring context
* how to refer to the instances you added and establish relationships among them

You can add beans in the context:
* Using the @Bean annotation

* Using stereotype annotations

* Programmatically

  AnnotationConfigApplicationContext class to create the Spring context instance
  ![b]( images/2.png "n")

## Using the @Bean annotation

1. Define a configuration class (annotated with @Configuration) 

2. Add to the context and annotate the method with the @Bean
3. Make Spring use the configuration class

![c]( images/3.png "n")
We use the configuration classes to define various Spring-related configurations for the project.

create a method that return the bean
Now we need to make sure Spring uses this configuration class when initializing its context

## Using stereotype annotations 

1. Using the @Component annotation, mark the classes for which you want Spring to add an instance to its context (in our case Parrot).

2. Using @ComponentScan annotation over the configuration class, instruct Spring on where to find the classes you marked.
![n](images/4.png)

By puting @Component annotation over the class, we instruct Spring to create an instance

**Spring doesn’t search for classes annotated with stereotype annotations**.Also, with the @ComponentScan annotation, we tell Spring where to look for these classes.

Now you told Spring the following:

Which classes to add an instance to its context (Parrot)

Where to find these classes (using @ComponentScan)
## Using the @Bean annotation vs Using stereotype annotations
Using the @Bean annotation
* add more instances of the same type
* doesn’t need to be defined in your app, we added a String and an Integer to the Spring context.

Using stereotype annotations
* one instance
* only to create beans of the classes your application owns

In  real-world scenarios you’ll use stereotype annotations as much as possible (because this approach implies writing less code), and you’ll only use the @Bean when you can’t add the bean otherwise

## Programmatically adding
Instance class dooesn’t need to be defined in your app, we added a String and an Integer to the Spring context but all are Singlton
you can dynamically add instance to context, which is not possible with @Bean or the stereotype annotations

if(condition) {  
registerBean(b1);

} else {

registerBean(b2);    

}
